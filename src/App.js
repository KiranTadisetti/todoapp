import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <p>ToDo app</p>
      <p><strong>A lot TO DO!!!</strong></p>
    </div>
  );
}

export default App;
